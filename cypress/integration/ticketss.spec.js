describe("Tickets", () => {
    beforeEach(() => cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html"));

    it("fills all the text input fields", () => {
        const firstName = "Lara";
        const lastName = "Monteiro";

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("monteiroslara@gmail.com");
        cy.get("#requests").type("Vegetariana");
        cy.get("#signature").type(`${firstName} ${lastName}`);
    });
    
    it("select two tickets", () => {
        cy.get("#ticket-quantity").select("2");
    });

    it("select vip", () => {
        cy.get("#vip").check();
    });

    it("select some checkboxes", () => {
        cy.get("#social-media").check();
    });
    it("has 'TICKETBOX'header's heading", () => {
        cy.get("header h1").should("contain", "TICKETBOX"); 
    }); 

    it("invalid email", () => {
        cy.get("#email")
        .as("email")
        .type("qualquer coisa");

        cy.get("#email.invalid").should("exist");

        cy.get("@email")
        .clear()
        .type("monteiroslara@gmail.com");

        cy.get("#email.invalid").should("not.exist");
    });

    it.only("fill all the fields", () => {
        const firstName = "Lara";
        const lastName = "Monteiro"; 
        const fullName = `${firstName} ${lastName}`;
        
        cy.get("header h1").should("contain", "TICKETBOX"); 
        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("monteiroslara@gmail.com");
        cy.get("#requests").type("Vegetariana");
        cy.get("#signature").type(fullName);
        cy.get("#ticket-quantity").select("2");
        cy.get("#vip").check();
        cy.get("#social-media").check();

        cy.get(".agreement p").should(
            "contain",
            `I, ${fullName}, wish to buy 2 VIP tickets.`
        );

        cy.get("#agree").check();
        cy.get("button[type='submit']")
            .as("submitButton")
            .should("not.be.disabled");

        cy.get("button[type='reset']").click();

        cy.get("@submitButton").should("be.disabled");
    }); 

    it("fills required fields using support command", () => {
        const customer = {
            firstName: "Lara",
            lastName: "Monteiro",
            email: "monteiroslara@gmail.com"
        };

        cy.fillMandatoryFields(customer);

        cy.get("button[type='submit']")
            .as("submitButton")
            .should("not.be.disabled");

        cy.get("#agree").uncheck();

        cy.get("@submitButton").should("be.disabled");
    });
});